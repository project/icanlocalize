<?php

module_load_include ( 'inc', 'icl_core', 'icl_core.constants' );
module_load_include ( 'inc', 'icl_core', 'icl_core.settings' );
module_load_include ( 'inc', 'icl_content', 'icl_content.contact_form' );


/**
 * Enter description here...
 *
 * @return unknown
 */
function icl_content_dashboard_filter_form() {
  $form = array(
    '#type' => 'fieldset',    
  );
  
  $languages = locale_language_list();
  $languages[] = t ('Language neutral');
  $default = _icl_content_default_value('language', language_default()->language);
  if ($default == '') {
    // handle language neutral setting
    $default = '0';
  }
  $form['language'] = array(
    '#title' => t('Show documents in'),
    '#type' => 'radios',    
    '#options' => $languages,
    '#default_value' => $default,
  );
  
  $form['translation'] = array(
    '#title' => t('Translation status'),
    '#type' => 'select',    
    '#options' => array(
      'all' => t('All documents'),
      'update' => t('Not translated or needs updating'),
      'complete' => t('Translation Complete'),
      'in_progress' => t('Translation in progess'),
    ),
    '#default_value' => _icl_content_default_value('translation', 'all'),
  );
  
  $form['further'] = array(    
    '#type' => 'markup',
    '#prefix' => '<div id="icl-dashboard-further" class="form-item"><label id="icl-dashboard-further-label">'.t('Filter further by').':',    
    '#suffix' => '</label></div>',
  );
  
  $form['further']['status'] = array(
    '#type' => 'fieldset',
  );
  $form['further']['status']['status_enabled'] = array(   
    '#type' => 'checkbox',
    '#default_value' => _icl_content_default_value('status_enabled', FALSE),
  );
  $form['further']['status']['status_status'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#options' => array(
      'status-1' => t('published'),
      'status-0' => t('not published'),
      'promote-1' => t('promoted'),
      'promote-0' => t('not promoted'),
      'sticky-1' => t('sticky'),
      'sticky-0' => t('not sticky'),
    ),
    '#default_value' => _icl_content_default_value('status_status', 'status-1'),
  );
  
  $form['further']['type'] = array(
    '#type' => 'fieldset',
  );
  $form['further']['type']['type_enabled'] = array(    
    '#type' => 'checkbox',
    '#default_value' => _icl_content_default_value('type_enabled', FALSE),
  );
  $types = array();
  foreach (node_get_types() as $type => $details) {
    $types[$type] = $details->name;    
  }
  // Add a block type
  $types['block'] = 'Block (i18n)';

  if (is_standard_contact_form_enabled()) {  
    // Add standard contact form
    $types['standard_contact_form'] = 'Contact Form';
  }
  
  $form['further']['type']['type_type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $types,
    '#default_value' => _icl_content_default_value('type_type', ''),
  );

  $form['further']['search'] = array(
    '#type' => 'fieldset',
  );
  $form['further']['search']['search_enabled'] = array(   
    '#type' => 'checkbox',
    '#default_value' => _icl_content_default_value('search_enabled', FALSE),
  );
  $form['further']['search']['search_text'] = array(
    '#title' => t('Title Search'),
    '#type' => 'textfield',
    '#default_value' => _icl_content_default_value('search_text', ''),
  );
  
  
  $form['op'] = array(
    '#value' => t('Update display'),
    '#type' => 'submit',
    '#submit' => array('icl_content_dashboard_filter_form_submit'),
  );
  
  return $form;
}

/**
 * Enter description here...
 *
 */
function icl_content_dashboard_filter_form_submit($form, &$form_state) {
  global $icl_content_dashboard_filter;
  
  $icl_content_dashboard_filter = array(
    'language' => $form_state['values']['language'],
    'translation' => $form_state['values']['translation'],
    'status_enabled' => $form_state['values']['status_enabled'] == 1,
    'status_status' => $form_state['values']['status_status'],
    'type_enabled' => $form_state['values']['type_enabled'] == 1,
    'type_type' => $form_state['values']['type_type'],
    'search_enabled' => $form_state['values']['search_enabled'] == 1,
    'search_text' => $form_state['values']['search_text'],
    
  );
  
  if ($icl_content_dashboard_filter['language'] == '0') {
    // handle language neutral setting
    $icl_content_dashboard_filter['language'] = '';
  }
  
  $_SESSION['icl_dashboard_filter'] = $icl_content_dashboard_filter;
  
  $form_state['rebuild'] = TRUE;
}

/**
 * Enter description here...
 *
 * @return unknown
 */
function icl_content_dashboard_nodes_form($dashboard_mode = TRUE) {
  $form = array(
    'title' => array(),
    'type' => array(),
    'status' => array(),
    'translation' => array(),
  );
  $form['#theme'] = 'icl_content_dashboard_nodes';
  $node_types = node_get_types();
  $nodes = array();
  if ($dashboard_mode) {
    $query = icl_content_dashboard_build_filter_query();
  } else {
    $query = icl_content_review_build_filter_query();
  }
  while ($node = db_fetch_object($query)) {
    if ($node->type == 'icl_block_marker') {
      $nid = 'block-'. $node->nid;
    } else if ($node->type == 'icl_contact_form_marker') {
      $nid = 'contact-'.$node->nid;
    } else {
      $nid = $node->nid;
    }
    $nodes[$nid] = '';

    if ($dashboard_mode) {
      $form['title'][$nid] = array('#value' => l($node->title, 'node/'.$node->nid));
    } else {
      // translation review mode
      // add a parameter to the link.
      $form['title'][$nid] = array('#value' => l($node->title, 'node/'.$node->nid, array('query' => array('review' => '1'))));
    }
     
    if ($node->type == 'icl_block_marker') {
      $title = $node->title;
      if ($title == "") {
        $block = block_box_get($node->nid);
    
        $block_data = _icl_content_extract_block($block);
        
        $title = _icl_content_get_block_title($block_data);
        
        if ($title == "(no title)") {
          $title .= " - " . $block['info'];
        }
      }
      $form['title'][$nid] = array('#value' => l($title, 'admin/build/block/configure/block/'.$node->nid));
      
      $form['type'][$nid] = array('#value' => t('Block (i18n)'));
      $form['status'][$nid] = array('#value' => t('Not applicable'));
      $form['translation'][$nid] = array('#value' => empty($node->language) ? t('Language neutral') : theme('icl_content_fancy_status', $node, TRUE));

    } else if ($node->type == 'icl_contact_form_marker') {
      $title = $node->title;
      $form['title'][$nid] = array('#value' => l($title, 'admin/build/contact'));

      $form['type'][$nid] = array('#value' => t('Contact Form'));
      $form['status'][$nid] = array('#value' => t('Enabled'));
      $form['translation'][$nid] = array('#value' => empty($node->language) ? t('Language neutral') : theme('icl_content_fancy_status', $node, TRUE));
    } else {
      
      $form['type'][$nid] = array('#value' => $node_types[$node->type]->name);
      $form['status'][$nid] = array('#value' => $node->status == 1 ? t('Published') : t('Unpublished'));
      $form['translation'][$nid] = array('#value' => empty($node->language) ? t('Language neutral') : theme('icl_content_fancy_status', $node, TRUE));
    }
    
    $form['translation'][$nid]['result-'.$nid] = array(
      '#type' => 'markup',
      '#prefix' => '</td></tr><tr id="tr-result-'.$nid.'" style="display:none;"><td></td><td colspan="4"><div id="result-'.$nid.'">',
      '#suffix' => '</div>',
      '#value' => '<!- This will be replaced ->',
    );
    /*$form['translation'][$nid]['word_count'.$nid] = array(
      '#type' => 'button',
      '#value' => 'Details',
      '#submit' => array('icl_content_icl_status'),
      '#ahah' => array(
        'path' => 'icl_content/icl_status/'.$nid,
        'wrapper' => 'result-'.$nid,
        'method' => 'replace',
        'effect' => 'fade',
        'progress' => array(
          'type' => 'throbber',
        )
        ),
    );
    */
    $ahah_binding = array(
      'url' => url('icl_content/icl_status/'.$nid),
      'event' => 'click',
      'wrapper' => 'result-'.$nid,
      'selector' => '#details-'.$nid,
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array(
        'type' => 'throbber',
      ),
      );
    
    if (strpos($form['translation'][$nid]['#value'], t ( 'Not translated yet' )) === FALSE) {
      $form['translation'][$nid]['#value'] = '<a href="#" id="details-'.$nid.'">'.$form['translation'][$nid]['#value'].'</a>';
    }
    
    drupal_add_js(array('ahah' => array('details-'.$nid => $ahah_binding)), 'setting');
    
  } 
  if ($dashboard_mode) {
    $form['nodes'] = array('#type' => 'checkboxes', '#options' => $nodes);
    if (isset($_GET['nid'])) {
      // select the single node
      $form['nodes']['#default_value'] = $nodes;
    }
  }
  
  return $form;
}

/**
 * Get the latest rids for each node
 * This will find the latest rid for each language for each node.
 */

function _icl_get_latest_rids() {
  
  $sql = "SELECT c.rid, cs.nid, c.target FROM {icl_core_status} c
          INNER JOIN {icl_content_status} cs ON cs.rid = c.rid
          WHERE 1
          
          UNION
          
          SELECT c.rid, bs.bid, c.target FROM {icl_core_status} c
          INNER JOIN {icl_block_status} bs ON bs.rid = c.rid
          WHERE 1";

  $rids = array();          
  $query = db_query($sql);
  while ( $result = db_fetch_object ( $query ) ) {
    if ( isset( $rids[$result->nid.$result->target] ) ) {
      if ( $result->rid > $rids[$result->nid.$result->target] ) {
        $rids[$result->nid.$result->target] = $result->rid;
      }
    } else {
      $rids[$result->nid.$result->target] = $result->rid;
    }
  }
  
  $rids = array_values($rids);
  return array_unique($rids);
}

function _icl_content_dashboard_filter_get_params() {
  $params = array();
  
  if ( isset ($_SESSION['icl_dashboard_filter']) ) {
    $params['language'] = $_SESSION['icl_dashboard_filter']['language'];
    $params['translation'] = $_SESSION['icl_dashboard_filter']['translation'];
    $params['status_status'] = $_SESSION['icl_dashboard_filter']['status_status'];
    $params['status_enabled'] = $_SESSION['icl_dashboard_filter']['status_enabled'];
    $params['type_type'] = $_SESSION['icl_dashboard_filter']['type_type'];
    if (isset($_SESSION['icl_dashboard_filter']['type_enabled'])) {
      $params['type_enabled'] = $_SESSION['icl_dashboard_filter']['type_enabled'];
    }
    if (isset($_SESSION['icl_dashboard_filter']['search_text'])) {
      $params['search_text'] = $_SESSION['icl_dashboard_filter']['search_text'];
    }
    if (isset($_SESSION['icl_dashboard_filter']['search_enabled'])) {
      $params['search_enabled'] = $_SESSION['icl_dashboard_filter']['search_enabled'];
    }
  }  
  return $params;
}

function _icl_content_dashboard_get_nodes_query(&$filter, $rids_to_use) {
  global $icl_content_dashboard_filter;

  $nids = array();
  
  if (!isset($icl_content_dashboard_filter['translation']) || $icl_content_dashboard_filter['translation'] == 'all') {
    $query = db_query('SELECT nid from {node} WHERE 1');
  }
  
  if (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'update') {
    $sql = 'SELECT
                n.nid from {node} n
            INNER JOIN {icl_content_status} cs ON n.nid = cs.nid 
            INNER JOIN {icl_core_status} c ON c.rid = cs.rid 
            INNER JOIN {icl_node} md ON md.nid = n.nid
                        
            WHERE md.md5 <> cs.md5 AND c.status = %d';
            
    if ($rids_to_use != '') {
      $sql .= " AND c.rid IN({$rids_to_use})";
    }

    // select nodes not tranlated and not managed by icl
    $sql = "SELECT
                n.nid 
              FROM
                {node} n
              WHERE
                (n.nid not in (select nid from {icl_content_status})) and n.tnid = 0 and language = '" . $icl_content_dashboard_filter['language'] . "' UNION " . $sql;
                        
    $query = db_query($sql, ICL_STATUS_SUCCESSFUL);
    
  } elseif (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'complete') {
    $potential_nids = array();

    $sql = "SELECT
                n.nid, c.status
	FROM
		{node} n
            INNER JOIN {icl_content_status} cs ON n.nid = cs.nid 
            INNER JOIN {icl_core_status} c ON c.rid = cs.rid 
            INNER JOIN {icl_node} md ON md.nid = n.nid

            WHERE md.md5 = cs.md5";
    
    // get the minimum status
    $complete_query = db_query($sql);
    while ( $result = db_fetch_object ( $complete_query ) ) {
      if (isset($potential_nids[$result->nid])) {
        $potential_nids[$result->nid] = min($potential_nids[$result->nid], $result->status);
      } else {
        $potential_nids[$result->nid] = $result->status;
      }
    }
    
    // Only use the ones that are complete
    foreach ($potential_nids as $nid => $status) {
      if ($status == ICL_STATUS_SUCCESSFUL) {
        $nids[] = $nid;
      }
    }
    
  } elseif (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'in_progress') {

    $sql = "SELECT
                n.nid, c.status
	FROM
		{node} n
            INNER JOIN {icl_content_status} cs ON n.nid = cs.nid 
            INNER JOIN {icl_core_status} c ON c.rid = cs.rid 
            INNER JOIN {icl_node} md ON md.nid = n.nid

            WHERE md.md5 = cs.md5 AND c.status < %d";
    
    $query = db_query($sql, ICL_STATUS_SUCCESSFUL);
    
  }

  if ($query) {  
    while ( $result = db_fetch_object ( $query ) ) {
      $nids[] = $result->nid;
    }
  }
  
  $nids_to_use = implode(",", $nids);
  if ($nids_to_use != '') {
    $filter['icl_where'] .= " AND n.nid IN({$nids_to_use})";
  } else {
    $filter['icl_where'] .= " AND n.nid IN(-1)";
  }
  
}

/**
 * Enter description here...
 *
 */
function icl_content_dashboard_build_filter_query($page_mode = true) {
  global $icl_content_dashboard_filter;
  
  if (isset($_GET['nid'])) {
    
    // just select the single node
    $sql = "SELECT n.nid, 
                n.title, 
                n.type, 
                n.language, 
                n.status,
                n.tnid
              FROM
                {node} n
              WHERE
                nid=%d" ;
    return db_query($sql, $_GET['nid']);
  }
  
  $node_types = _icl_content_translatable_node_types();
  
  $rids = _icl_get_latest_rids();
  $rids_to_use = implode(",", $rids);
  
  $filter = array();  
  $args = array();  
  $args[] = _icl_content_default_value('language', language_default()->language );
  
  if ($icl_content_dashboard_filter == null) {
    $icl_content_dashboard_filter = _icl_content_dashboard_filter_get_params();
  }
  
  _icl_content_dashboard_get_nodes_query($filter, $rids_to_use);
  
  if (isset($icl_content_dashboard_filter['status_status']) && $icl_content_dashboard_filter['status_enabled']) {
    list($column, $value) = explode('-', $icl_content_dashboard_filter['status_status']);
    if (in_array($column, array('status', 'promote', 'sticky'))) {
      $filter['where'] .= " AND n.".$column." = ".$value;
    }
  }

  $type_filter = "";  
  if (isset($icl_content_dashboard_filter['type_type']) && isset($icl_content_dashboard_filter['type_enabled'])) {
    if ($icl_content_dashboard_filter['type_enabled']) {
      $filter['where'] .= " AND n.type = '". $icl_content_dashboard_filter['type_type'] . "'";
      $type_filter = $icl_content_dashboard_filter['type_type'];
    }
  }
  
  if (isset($icl_content_dashboard_filter['search_text']) && isset($icl_content_dashboard_filter['search_enabled'])) {
    if ($icl_content_dashboard_filter['search_enabled'] && $icl_content_dashboard_filter['search_text'] != '') {
      $filter['where'] .= " AND n.title LIKE '%%". $icl_content_dashboard_filter['search_text'] . "%%'";
    }
  }
  
  $sql  = "SELECT 
             n.nid, 
             n.title, 
             n.type, 
             n.language, 
             n.status,
             n.tnid
           FROM 
             {node} n
           WHERE
             n.type IN ('".implode("', '", $node_types)."') AND 
             n.language = '%s'"
           .$filter['icl_where']
           .$filter['where'];
  
  
  // add blocks if required
  
  if (module_exists ( 'i18nblocks' )) {
    if (($type_filter == '' || $type_filter == "block") && $icl_content_dashboard_filter['language'] == "en") {
      icl_content_add_block_form_query($sql, $icl_content_dashboard_filter, $rids_to_use);
    }
  }
  
  if (($type_filter == '' || $type_filter == "standard_contact_form") && $args[0] == "en") {
    icl_content_add_contact_form_query($sql, $icl_content_dashboard_filter);
  }
  
  if ($page_mode) {
    $count_query = "SELECT COUNT(*) FROM (" . $sql . ") tblDerived";
  
    return pager_query($sql, 30, 0, $count_query, $args);
  } else {
    return db_query($sql, $args);
  }
}


/**
 * Enter description here...
 *
 * @return unknown
 */
function icl_content_dashboard_action_form(&$form_in, $translation_options_only = false) {
  
  icl_core_add_thickbox();
  
  $form = array();
  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translation options'),
    '#attributes' => array('id' => 'dashboard-actions'),
  );
  $languages = array();
  $no_languages = array();
  $pending_languages = array();
  $no_translator_languages = array();
  $defaults = array();
  $selected_langauge = _icl_content_default_value('language', language_default()->language);
  $selected_langauge = icl_core_get_language_name($selected_langauge);

  $platform_info = array();
  $clear_cache = FALSE;
  if (isset($_GET['icl_refresh_langs'])){
    $clear_cache = TRUE;
  }
  $ican_languages = icl_core_available_languages ( $clear_cache, $platform_info, TRUE );
  $icl_core_languages = variable_get('icl_core_languages', array());

  // check for a change of url
  if (icl_core_is_icl_stored_url_ok($platform_info)) {
    unset ( $form_in['icl_url_changed'] );
  } else {
    icl_core_get_url_changed_message ( $form_in['icl_url_changed'], $platform_info );
  }
  

  if ($selected_langauge == null) {
    $form['operations']['message'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="status">',
      '#suffix' => '</div>',
      '#value' => t('Content that is language neutral cannot be sent for translation. Edit the content and set the language if you want to send it for translation'),
    );
  } else {
    
    if (isset($icl_core_languages['langs'][$selected_langauge])) {
      foreach ($icl_core_languages['langs'][$selected_langauge] as $lang) {
        if($icl_core_languages['available_translators'][$selected_langauge][$lang] > 0) {
          $codes = icl_core_get_language_codes($lang);
          foreach($codes as $code) {
            $languages[$code] = t('Translate to @lang ', array('@lang' => icl_core_get_drupal_language_name($code))) . icl_get_language_status_text($ican_languages, $platform_info, language_default()->language, $code);
          }
          
          if($icl_core_languages['applications'][$selected_langauge][$lang] > 0 && $icl_core_languages['have_translators'][$selected_langauge][$lang] == 0) {
            // Need to choose a translator
            foreach($codes as $code) {
              $pending_languages[$code] = t('Translate to @lang ', array('@lang' => icl_core_get_drupal_language_name($code))) . icl_get_language_status_text($ican_languages, $platform_info, language_default()->language, $code);
            }
          }
          
        } else {
          $codes = icl_core_get_language_codes($lang);
          foreach($codes as $code) {
            $no_languages[$code] = t('Translate to @lang ', array('@lang' => icl_core_get_drupal_language_name($code))) . icl_get_language_status_text($ican_languages, $platform_info, language_default()->language, $code);
            $no_translator_languages[] = icl_core_get_drupal_language_name($code);
          }
        }
      }
    }
    
    foreach($languages as $code => $value) {
      $defaults[] = $code;
    }
  
    if (sizeof($languages) == 0) {
      $form['operations']['no_langs'] = array(
        '#type' => 'markup',
        '#value' => '<b>' . t('There are no translators assigned to any of your target languages.') . '</b>',
      );
    }
    $form['operations']['targets'] = array(
      '#type' => 'checkboxes',
      '#options' => $languages,
      '#default_value' => $defaults,
    );
    if (sizeof($no_languages) > 0 || sizeof($pending_languages) > 0) {
      $html = '';
      
      if (sizeof($pending_languages) > 0) {
        $html .= '<b>' . t('Choose your translators') . '</b>';
        
        $html .= "<ul>\n";
        foreach ($pending_languages as $text) {
          $html .= '<li class="icl_no_translator">' . $text . "</li>\n";
        }
        $html .= "</ul>\n";
      }
      
      if (sizeof($no_languages) > 0) {
        if (sizeof($no_languages) == 1) {
          $html .= '<b>' . t('No translators available to this language') . '</b>';
        } else {
          $html .= '<b>' . t('No translators available to these languages') . '</b>';
        }
        $html .= '<br />' . t('You will only be able to send translations to languages for which translators are available.');
        
        $html .= "<ul>\n";
        foreach ($no_languages as $text) {
          $html .= '<li class="icl_no_translator">' . $text . "</li>\n";
        }
        $html .= "</ul>\n";
      }
      if (!$translation_options_only) {
        $form_in['status_messages']['#access'] = TRUE;
        $form_in['status_messages']['#value'] .= $html;
      }
      
    }
    
    if (sizeof($languages) == 0 && sizeof($no_translator_languages) == 0) {
      $form['operations']['message'] = array(
        '#type' => 'markup',
        '#prefix' => '<div class="status">',
        '#suffix' => '</div>',
        '#value' => t('No target languages have been set for translating from !language', array('!language' => icl_core_get_drupal_language_names_from_ican_language($selected_langauge))),
      );
    } else if (sizeof($languages) != 0) {
      $form['operations']['translate_request'] = array(
        '#type' => 'submit',
        '#validate' => array('icl_content_dashboard_action_validate'),
        '#submit' => array('icl_content_dashboard_action_submit'),
        '#value' => t('Translate selected documents'),
      );
    }
    
    
    if ($icl_core_languages['balance'] < variable_get('icl_balance_warning', 50)) {
      if (!$translation_options_only) {
        $form_in['status_messages']['#access'] = TRUE;
        $form_in['status_messages']['#value'] .= '<b>'. t('Your balance in ICanLocalize is running low. To allow translations to continue, visit your !finance page and deposit additional funds.', array('!finance' => icl_create_icl_popup_link(ICL_FINANCE_LINK) . 'ICanLocalize finance</a>')) . '</b><br />';
      }
    }
    
    
    
  }
  
  if (!$translation_options_only) {
    $form['cost_estimate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Translation cost estimate'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['cost_estimate']['result_count'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="result-count">',
      '#suffix' => '</div>',
      '#value' => '<!- This will be replaced ->',
    );
    $form['cost_estimate']['word_count'] = array(
      '#type' => 'button',
      '#value' => 'Produce word count for untranslated contents',
      //'#attributes' => array('onclick' => "javascript:on_word_count_button();return false;"),
      '#submit' => array('icl_content_word_count'),
      '#ahah' => array(
        'path' => 'icl_content/word_count',
        'wrapper' => 'result-count',
        'method' => 'replace',
        'effect' => 'fade',
        'progress' => array(
          'type' => 'throbber',
        )
        ),
    );
  }  
  return $form;
}

/**
 * Enter description here...
 *
 * @param unknown_type $form
 * @return unknown
 */
function theme_icl_content_dashboard_translation_selection($form) {
  $output = '';  
  foreach ($form['#options'] as $key => $value) {
    $output .= '  <div id="edit-targets-'.$key.'-wrapper" class="form-item">';
    $output .= '    <label for="targets['.$key.']" class="option">'.$value.'</label>';
    $output .= '    <input id="edit-targets-'.$key.'" class="form-checkbox" type="checkbox" value="'.$key.'" name="targets['.$key.']"/>';
    $output .= '  </div>';    
  }
  
  return $output;  
}

/**
 * Enter description here...
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function icl_content_dashboard_action_validate($form, &$form_state) {
  $nodes = array_filter($form_state['values']['nodes']);
  if (count($nodes) == 0) {
    form_set_error('', t('No items selected.'));
  }
  
  $targets = array_filter($form_state['values']['targets']);
  if (count($targets) == 0) {
    form_set_error('', t('No translation targets were selected.'));
  }  
}

/**
 * Enter description here...
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function icl_content_dashboard_action_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
    
  // Filter out unchecked nodes
  $content_to_translate = array_filter($form_state['values']['nodes']);
  $nodes = array();
  $blocks = array();
  $contact_form = False;
  foreach($content_to_translate as $content) {
    if (strpos($content, 'block-') === 0) {
      $blocks[] = substr($content, 6);
    } else if (strpos($content, 'contact-') === 0) {
      $contact_form = True;
    } else {
      $nodes[] = $content;
    }
  }

  $langcodes = array();  
  foreach($form_state['values']['targets'] as $key => $value){
      if ($key === $value) {
        $langcodes[] = $value;
      }
  }
  icl_content_dashboard_filter_form_submit($form, $form_state);
  
  icl_content_translate_posts($nodes, $langcodes);
  icl_content_translate_blocks($blocks, $form_state['values']['language'], $langcodes);
  if ($contact_form) {
    icl_content_translate_standard_contact_form($form_state['values']['language'], $langcodes);
  }
}

function icl_content_dashboard_email_validate($form, &$form_state) {
  variable_set('icl_core_email_address', $form_state['values']['icl_core_email_address']);
  
  $session_id = _icl_get_session_id();
  
  if ($session_id == NULL) {
    variable_set('icl_core_email_address', '');
    form_set_error('', '<b>' . t('Cannot access the ICanLocalize server with this email address') . '</b>');
  }
  
}

function icl_content_dashboard_email_submit($form, &$form_state) {
  variable_set('icl_core_email_address', $form_state['values']['icl_core_email_address']);
}

/**
 * Enter description here...
 *
 * @return unknown
 */
function icl_content_dashboard() {
  
  $form = array();
  
  if (variable_get ( 'icl_core_website_id', '' ) != '' && variable_get ( 'icl_core_accesskey', '' ) != '') {
    if (variable_get('icl_core_email_address', '') ==  '') {
  
      // we probably don't have an email address
      $form ['icanlocalize_setup'] = array (
         '#type' => 'fieldset', 
         '#title' => t ( '<b>Before continuing</b>' ), 
         '#collapsible' => false );
      $form ['icanlocalize_setup'] ['message'] = array (
            '#type' => 'markup',
            '#prefix' => '<div id="icl_core_account_message">',
            '#suffix' => '</div>',
            '#value' => t("Please enter your email address that you used to setup your ICanLocalize Account"));
      $form ['icanlocalize_setup'] ['icl_core_email_address'] = array (
          '#type' => 'textfield', 
          '#title' => t ( 'Email address' ), 
          '#default_value' => variable_get('icl_core_email_address', ''), 
          '#required' => TRUE);
  
      $form['icanlocalize_setup']['Save'] = array(
        '#type' => 'submit',
        '#validate' => array('icl_content_dashboard_email_validate'),
        '#submit' => array('icl_content_dashboard_email_submit'),
        '#value' => t('Save email address'),
      );
  
      return $form;
    }
  }
  
  $i18n_not_installed = check_i18n_installed();
  if (sizeof($i18n_not_installed) > 0) {
    $form['i18n_errors'] = array (
      '#type' => 'fieldset', 
      '#title' => t ( 'Missing i18n modules.' ), 
      '#collapsible' => false );
  
    foreach($i18n_not_installed as $id => $module) {
      $form ['i18n_errors'] [$module['module']] = array (
          '#type' => 'markup', 
          '#prefix' => '<div class="error">', 
          '#suffix' => '</div>', 
          '#value' => $module['message'] );
    }
  }

  $form['icl_url_changed'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="icl_red_box">',
    '#suffix' => '</div>',
    '#value' => '',
  );

  $form = icl_content_dashboard_add_title_warning($form);
  
  $form['status_messages'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="icl_cyan_box">', 
    '#suffix' => '</div>', 
    '#value' => '',
    '#access' => FALSE,
    );

  $form = icl_core_add_reminders($form);

  $form['icl_support'] =  array(
    '#type' => 'markup',
    '#prefix' => '<div class="icl_cyan_box">',
    '#suffix' => '</div>',
    '#value' => icl_core_get_support_help(),
  );

  if (isset($_GET['nid'])) {
    $form['icl_show_all'] =  array(
      '#type' => 'markup',
      '#prefix' => '<div class="icl_cyan_box">',
      '#suffix' => '</div>',
      '#value' => t('<a href="@url">Show all documents</a>', array('@url' => url('admin/content/icl-dashboard'))),
    );
  } else {
    $form['filter'] = icl_content_dashboard_filter_form();
  }

  $form['nodes'] = icl_content_dashboard_nodes_form();
  $form['pager'] = array('#value' => theme('pager', NULL, 30, 0));
  $form['action'] = icl_content_dashboard_action_form($form);
  // A small hack to include the CSS and JS
  $form ['#validate'] [] = 'icl_content_dashboard_add_js';
  icl_content_dashboard_add_js ($form);

  // repeat the support message at the bottom.
  $form['icl_support2'] = $form['icl_support'];
  if (isset($_GET['nid'])) {
    // remove the top support message
    unset($form['icl_support']);
  }
  

  return $form;
}

function icl_content_dismiss_warning() {
  if ($_POST['command'] == 'dismiss_all') {
    variable_set('icl_dismiss_title_warning', TRUE);
  }

  if ($_POST['command'] == 'dismiss_type') {
    variable_set('icl_dismiss_title_warning_type_'.$_POST['type'], TRUE);
  }
}

function icl_content_dashboard_add_title_warning($form) {
  $warning = array();

  if (!variable_get('icl_dismiss_title_warning', FALSE)) {
    
    $query = db_query("SELECT * FROM {node_type} WHERE 1");
    while($result = db_fetch_object($query)) {
  
      if (!variable_get('icl_dismiss_title_warning_type_'. $result->type, FALSE)) {
        $enabled = variable_get('language_content_type_'. $result->type, 0);
        if ($enabled == 2) {
          $translate_fields = variable_get ( 'icl_content_node_type_fields', array ( $result->type => array () ) );
          $translate_fields = $translate_fields[$result->type];
          //if (is_array($translate_fields) && !in_array('link_title', $translate_fields)) {
          if (empty($translate_fields)) {
            $link = url('admin/content/node-type/'.str_replace('_', '-', $result->type));
            $warning[] = '<tr id="icl_row-' . $result->type . '"><td>' . $result->type . '</td><td><a href="' . $link . '">' . t('edit') . '</a></td><td><a id="icl_menu_dismiss-' . $result->type . '" href="#">' . t('dismiss') . '</a></td></tr>';
          }
        }
      }
    }
  }
  
  $message = '';

  if (!variable_get('icl_status_check_done', FALSE)) {
    $message .= '<img src="/misc/watchdog-warning.png">&nbsp;';
    $link = url('admin/content/icl-check');
    $message .= t('<b>Status check</b> - Ensure you have everything setup before sending translations. Perform a <a href="@link">Status check</a>', array('@link' => $link)) . '<br>';
  }
  
  if (sizeof($warning)) {
    $message .= '<img src="/misc/watchdog-warning.png">&nbsp;';
    $message .= t('<b>NOTE - Menu translation:</b> The following content types wont have their menus sent for translation. <a id="icl_menu_dismiss_all" href="#">dismiss all</a>') . '<br>';
    $message .= '<table><thead><tr><th>' . t('Node type') . '</th><th>' . t('Go to node type') . '</th><th>' . t('Dismiss warning') . '</th></tr></thead><tbody>';
    $message = $message . implode('', $warning) . '</tbody></table>';
  }
  
  if (sizeof($message) > 0) {
    $form['menu_title_warning_message'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="icl_menu_warning" class="icl_cyan_box">', 
      '#suffix' => '</div>', 
      '#value' => $message,
      );
  }  
  
  
  return $form;
}

function icl_content_dashboard_add_js($form) {
  //foreach($form['action']['operations'] as $key => $value) {
    //if (strpos($key, 'no_translator_') === 0) {
        
      $js = drupal_get_js();
      if (strpos($js, 'icl_dashboard.js') === FALSE) {
        
        // add a call to refresh the the icanlocalize have_translator states for languages.
        $ican_url = url('icl_core/refresh');
        $ican_dismiss_warning_url = url('icl_content_type/dismiss_warning');
        
        drupal_add_js(
                      array('ican_fetch' =>
                            array('ican_url' => $ican_url,
                                'ican_dismiss_warning_url' => $ican_dismiss_warning_url)),
                            'setting');
        drupal_add_js(
                      array('ican_ajax' =>
                            array('ican_dismiss_warning_url' => $ican_dismiss_warning_url)),
                            'setting');
        drupal_add_js(
                      array('ican_word_count' =>
                            array('ican_word_count_url' => url('icl_content/word_count'))),
                            'setting');
        drupal_add_js ( drupal_get_path ( 'module', 'icl_content' ) . '/js/icl_dashboard.js' );
        drupal_add_js ( drupal_get_path ( 'module', 'icl_core' ) . '/js/icl_reminders.js' );
      }
      return;
    //}
  //}
}

/**
 * Enter description here...
 *
 * @return unknown
 */
function _icl_content_translatable_node_types() {
  if (module_exists('content')) {
    $node_types = array_keys(content_types());
  } else {
    $node_types = array_keys(node_get_types());
  }

  foreach ($node_types as $key => $type) {
    $translatable = variable_get('language_content_type_'.$type, false);
    if (!$translatable) {
      unset($node_types[$key]);
    }
  }
  
  return $node_types;
}

/**
 * Helper to set the default_value for the form.
 *
 * @param unknown_type $key
 * @param unknown_type $default
 * @return unknown
 */
function _icl_content_default_value($key, $default) {
  global $icl_content_dashboard_filter;
  
  if (isset($icl_content_dashboard_filter[$key])) {
    return $icl_content_dashboard_filter[$key];
  }
  else if (isset($_SESSION['icl_dashboard_filter'][$key])) {
    return $_SESSION['icl_dashboard_filter'][$key];    
  }
  else {
    return $default;
  }
}

/**
 * Enter description here...
 *
 * @param unknown_type $form
 */
function theme_icl_content_dashboard_nodes($form) {
  drupal_add_css(drupal_get_path('module', 'icl_content').'/css/dashboard.css');
  
  $has_posts = count(element_children($form['title'])) > 0;
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Title'), t('Type'), t('Status'), t('Translation'));
  
  if ($has_posts) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      if (isset($form['nodes']['#default_value']) && array_key_exists($key, $form['nodes']['#default_value'])) {
        // Check the checkbox.
        // ????Why doesn't drupal do this????
        $form['nodes'][$key]['#value'] = 1;
      }
      $row[] = drupal_render($form['nodes'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['type'][$key]);
      $row[] = drupal_render($form['status'][$key]);
      $row[] = drupal_render($form['translation'][$key]);      
      $rows[] = $row;
    }
    
  }
  else {
    $rows[] = array(array('data' => t('No content available.'), 'colspan' => '6'));
  }
  
  $output = theme('table', $header, $rows);
  
  return $output;
}

function _icl_content_get_review_languages() {
  $languages = array();
  $languages[] = t ('All');

  $langs = _icl_core_available_targets();
  
  foreach($langs as $code => $lang) {
    $languages[$code] = icl_core_get_drupal_language_name($code);
  }
  return $languages;
}

function icl_content_translation_review() {
  $form = array();
  
  $form = icl_core_add_reminders($form);
  icl_core_add_thickbox();

  $form['icl_support'] =  array(
    '#type' => 'markup',
    '#prefix' => '<div class="icl_cyan_box">',
    '#suffix' => '</div>',
    '#value' => icl_core_get_icl_support(),
  );
  
  $form['message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Completed Translations'),
    '#value' => "<br>" . t('The following translations have been received and are waiting for review'),
  );

  $languages = _icl_content_get_review_languages();
  
  $default = _icl_content_review_default_value('review_language', array_shift(array_keys($languages)));
  if ($default == 'All') {
    // handle language neutral setting
    $default = '0';
  }
  $form['message']['review_language'] = array(
    '#title' => t('Show documents in'),
    '#type' => 'radios',    
    '#options' => $languages,
    '#default_value' => $default,
  );
  $form['message']['op'] = array(
    '#value' => t('Update display'),
    '#type' => 'submit',
    '#submit' => array('icl_content_review_filter_form_submit'),
  );

  $form['nodes'] = icl_content_dashboard_nodes_form(FALSE);
  $form['pager'] = array('#value' => theme('pager', NULL, 10, 0));
    
  return $form;
}

/**
 * Enter description here...
 *
 */
function icl_content_review_filter_form_submit($form, &$form_state) {
  global $icl_content_review_filter;
  
  $icl_content_review_filter = array(
    'review_language' => $form_state['values']['review_language'],
  );
  if ($icl_content_review_filter['review_language'] == '0') {
    // handle all language setting
    $icl_content_dashboard_filter['review_language'] = '';
  }
  $form_state['rebuild'] = TRUE;
}

function _icl_content_review_default_value($key, $default) {
  global $icl_content_review_filter;
  
  if (isset($icl_content_review_filter[$key])) {
    return $icl_content_review_filter[$key];
  }
  else if (isset($_GET[$key])) {
    return $_GET[$key];    
  }
  else {
    return $default;
  }
}

function icl_content_review_build_filter_query() {

  $lang = _icl_content_review_default_value('review_language', array_shift( array_keys(_icl_content_get_review_languages()) ));
  
  $node_types = _icl_content_translatable_node_types();
  
  $filter = array();  
  
  $filter['join'] .= ' INNER JOIN {icl_content_status} cs ON n.tnid = cs.nid 
                       INNER JOIN {icl_core_status} c ON c.rid = cs.rid';
  $filter['where'] .= " AND c.status = " . ICL_STATUS_SUCCESSFUL;
  
  if ($lang != "0") {
    $filter['where'] .= " AND n.language = '" . $lang . "'";
  }
  $sql  = "SELECT 
             distinct n.nid, 
             n.title, 
             n.type, 
             n.language, 
             n.status,
             n.tnid
           FROM 
             {node} n"
           .$filter['join'].
          " WHERE
             n.type IN ('".implode("', '", $node_types)."') AND 
             n.tnid != n.nid AND
             c.review_required = 1 AND
             c.target = n.language"
           .$filter['where'];
  
  $count_query = preg_replace(array('/SELECT.*?FROM /As', '/ORDER BY .*/'), array('SELECT COUNT(distinct n.nid) FROM ', ''), $sql);
  
  return pager_query($sql, 10, 0, $count_query);
}
