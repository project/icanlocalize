<?php
/*
 * $Id: icl_content.block.inc
 * 
 * @file ICanLocalize content block handling
 */

module_load_include ( 'inc', 'icl_content', 'icl_content.images' );

function icl_content_save_block_translation($bid, $code, $data) {
  $block_original = block_box_get($bid);
  
  foreach ($data as $item) {
    if ($item['type'] == 'title') {
      $title = $item['data'];
      
      // find the original in the locales_source
      
      $query = db_query("SELECT lid FROM {locales_source} WHERE source = '%s' and textgroup = 'blocks'", $item['original_data']);
      while ($result = db_fetch_array($query)) {
        $lid = $result['lid'];
      
        if(!empty($lid)) {
          icl_content_update_locale_translation($lid, $code, $title);
        } else {
          watchdog ( 'icl_content', 'Nonexistent originating block title ' . $block_original['info'], WATCHDOG_ERROR );
          //return FALSE;
          // Don't return false as the user may have changed the original text of the block and that is the reason it can't be found.
        }
      }
    }
    if ($item['type'] == 'Block titles') {
      foreach($item['data'] as $index => $title) {
        
        // find the original in the locales_source
        
        $query = db_query("SELECT lid FROM {locales_source} WHERE source = '%s' and textgroup = 'blocks'", $item['original_data'][$index]);
        while ($result = db_fetch_array($query)) {
          $lid = $result['lid'];
        
          if(!empty($lid)) {
            icl_content_update_locale_translation($lid, $code, $title);
          } else {
            watchdog ( 'icl_content', 'Nonexistent originating block title ' . $block_original['info'], WATCHDOG_ERROR );
            //return FALSE;
            // Don't return false as the user may have changed the original text of the block and that is the reason it can't be found.
          }
        }
      }
    }
    if ($item['type'] == 'body') {
      $all_links_fixed = FALSE;
      $body = _icl_content_fix_links_in_text($item['data'], "en", $code, $all_links_fixed);
      
      $images_in_original = _icl_content_get_image_paths($body);
      
      $block_changed = FALSE;
      $file_status = array();
      icl_content_apply_image_replacement_block($images_in_original,
                                          $body,
                                          $bid,
                                          $code,
                                          $block_changed,
                                          $file_status);
      

      $query = db_query("SELECT lid FROM {locales_source} WHERE source = '%s' and textgroup = 'blocks'", $block_original['body']);
      while ($result = db_fetch_array($query)) {
        $lid = $result['lid'];

        if(!empty($lid)) {
          icl_content_update_locale_translation($lid, $code, $body);
        } else {
          watchdog ( 'icl_content', 'Nonexistent originating block body ' . $block_original['body'], WATCHDOG_ERROR );
          //return FALSE;
          // Don't return false as the user may have changed the original text of the block and that is the reason it can't be found.
        }
      }
    }
  }
  
  return TRUE;
}


/**
 * calculate the md5 value of a block using title and body and other data.
 *
 */

function icl_content_calculate_block_md5($block) {
  $data = _icl_content_extract_block($block);
  
  return icl_content_calculate_md5_from_data($data);
  
}

function _icl_content_is_block_translation_in_progess($block) {
  $rid = db_result(db_query("SELECT rid FROM {icl_block_status} where bid='%d'", $block['bid']));
  
  if($rid === FALSE) {
    return FALSE;
  }
  
  $result = db_result(db_query("SELECT * FROM {icl_core_status} where rid='%d' and status <> '%d'", $rid, ICL_STATUS_SUCCESSFUL));
  
  return $result !== FALSE;
}

function _icl_content_is_block_i18n_enabled($block) {
  $bid = db_result(db_query("SELECT ibid FROM {i18n_blocks} WHERE delta = %d", $block['bid']));
  
  return $bid !== FALSE;
}

function _icl_content_is_block_i18n_with_translation($block) {
  $language = db_result(db_query("SELECT language FROM {i18n_blocks} WHERE delta = %d", $block['bid']));
  
  return $language == "";
}

/**
 * Sends blocks off for translation
 *
 * @param object $block
 * @return array
 */
  
function icl_content_translate_blocks($blocks, $source_lang, $dest_langs) {
  
  foreach($blocks as $bid) {
    $block = block_box_get($bid);
    
    $targets = _icl_get_langs_not_in_progress($bid, 'block', $block, $source_lang, $dest_langs);

    if (sizeof($targets) == 0) {
      continue;
    }
    
    if (!_icl_content_is_block_i18n_enabled($block)) {
      drupal_set_message ( t ( 'error: The block is not managed by i18n block module. !block', array('!block' => $block['info'])), 'error');
      continue;
    }
    if (!_icl_content_is_block_i18n_with_translation($block)) {
      drupal_set_message ( t ( 'error: The block is not set to All languages (with translations). !block', array('!block' => $block['info'])), 'error');
      continue;
    }
    
    // Have we translated this before.
    
    $block_md5 = icl_content_calculate_block_md5($block);

    $langs = icl_core_available_languages ();
    $langs_targets = _icl_core_available_targets_for_origin ( $source_lang );

    $previous_rids = icl_content_get_rids($bid, 'block');

    $targets = _icl_content_get_langs_needing_update($targets, $langs_targets, $previous_rids, 'block', $block_md5);

    if (count ( $targets )) {
      $origin = $langs [$source_lang];
      $data = _icl_content_extract_block($block);
      
      // create a separate cms_request for each language if possible
      // if the languages have previously been sent as a combined cms_request
      // we need to do the same this time.
      
      $grouped_targets = _icl_group_targets_by_rid($bid, 'block', $source_lang, $targets, $previous_rids);

      $rids_sent = icl_core_send_content_for_translation('icl_content',
                                            $data,
                                            $origin,
                                            $grouped_targets,
                                            $previous_rids,
                                            "",
                                            $block['info']
                                            );
      
      
      foreach ($rids_sent as $rid) {
        if ($rid != 0) {
          db_query ( "INSERT INTO {icl_block_status} VALUES(%d, %d, %d, '%s')", $rid, $block['bid'], time (), $block_md5 );
          db_query ( "INSERT INTO {icl_block} VALUES(%d, '%s', '%d')", $block['bid'], $block_md5, FALSE );
        }
      }      
    }
  }
}
    

/**
 * Extracts valuable data from a block object.
 *
 * @param object $block
 * @return array
 */
function _icl_content_extract_block($block) {
  $query = db_query("SELECT title FROM {blocks} WHERE delta = %d", $block['bid']);
  $title = array();
  while ($result = db_fetch_array($query)) {
    if (strlen($result['title']) > 0 && !in_array($result['title'], $title)) {
      $title[] = $result['title'];
    }
  }

  // check the format to see if line breaks need to be converted
  $formats = filter_list_format($block['format']);
  $line_break_converter_found = false;
  foreach($formats as $format) {
    if ($format->delta == 1) {
      $line_break_converter_found = True;
    }
  }
  if (!$line_break_converter_found) {
    $filter_line_breaks = 0;
  } else {
    $filter_line_breaks = 1;
  }
  
  $data = array (
      array (
          'type' => 'bid', 
          'translate' => 0, 
          'text' => $block['bid'] ), 
      array (
          'type' => 'body', 
          'translate' => 1, 
          'text' => $block['body'],
          'filter_line_breaks' => $filter_line_breaks),
      
      
      );

  if (sizeof($title) == 1) {
    $data[] = array (
          'type' => 'title', 
          'translate' => 1, 
          'text' => $title[0]);
  } else if (sizeof($title) > 1) {
    $data[] = array (
          'type' => 'Block titles', 
          'translate' => 1, 
          'text' => $title,
          'format' => 'csv');
    
  }
  
  return $data;
}

function _icl_content_get_block_title($block_data) {
  $block_title = t('(no title)');
  foreach($block_data as $data) {
    if ($data['type'] == 'title') {
      $block_title = $data['text'];
    }
  }
  
  return $block_title;
}

function icl_content_add_block_form_query(&$sql, $icl_content_dashboard_filter, $rids_to_use) {

  // We don't know if a block has changed from the core status as we don't hook any saving of blocks.
  // We need to calculate each time.
  
  $query = db_query("SELECT delta FROM {i18n_blocks} WHERE language = ''");
  while ( $bid = db_fetch_object ( $query ) ) {
    $block = block_box_get($bid->delta);
    $block_md5 = icl_content_calculate_block_md5($block);
    
    db_query("UPDATE {icl_block} SET md5 = '%s' WHERE bid = %d", $block_md5, $bid->delta);
  }

  $current_theme = variable_get('theme_default', 'none');
  $sql = $sql . " UNION SELECT DISTINCT(b.delta), b.title, 'icl_block_marker', 'en', 'visible', '0' FROM {i18n_blocks} i18b";
  $join = " join {blocks} b ";
  $where = " where i18b.language = '' and b.delta = i18b.delta and b.module = 'block' and b.theme = '".$current_theme."'";
  if (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'update') {
    // We need to get the blocks that require update
    
    $join .= ' INNER JOIN {icl_block_status} bs ON b.delta = bs.bid 
                       INNER JOIN {icl_core_status} c ON c.rid = bs.rid 
                       INNER JOIN {icl_block} md ON md.bid = b.delta';
    $where .= " AND md.md5 <> bs.md5 AND c.status = " . ICL_STATUS_SUCCESSFUL;
    if ($rids_to_use != '') {
      $where .= " AND c.rid IN({$rids_to_use})";
    }
  }
  
  if (isset($icl_content_dashboard_filter['search_text']) && isset($icl_content_dashboard_filter['search_enabled'])) {
    if ($icl_content_dashboard_filter['search_enabled'] && $icl_content_dashboard_filter['search_text'] != '') {
      $where .= " AND b.title LIKE '%%". $icl_content_dashboard_filter['search_text'] . "%%'";
    }
  }
  
  
  $sql = $sql . $join . $where;

  // filter bids

  if (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'update') {
    // also add the blocks that are new and not in icl_block yet
    $sql .= " UNION SELECT
                  DISTINCT(b.delta), b.title, 'icl_block_marker', 'en', 'visible', '0'
                FROM
                  {i18n_blocks} i18b
                
                JOIN
                  {blocks} b
                 
                WHERE
                  i18b.language = '' and b.delta = i18b.delta and b.module = 'block' AND
                  (b.delta not in (select bid from {icl_block_status}))";
  } elseif (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'complete') {
    $potential_bids = array();

    $block_status_sql = "SELECT
                b.bid, c.status
	FROM
		{icl_block} b
            INNER JOIN {icl_block_status} bs ON b.bid = bs.bid 
            INNER JOIN {icl_core_status} c ON c.rid = bs.rid 

            WHERE b.md5 = bs.md5";
    
    // get the minimum status
    $complete_query = db_query($block_status_sql);
    while ( $result = db_fetch_object ( $complete_query ) ) {
      if (isset($potential_bids[$result->bid])) {
        $potential_bids[$result->bid] = min($potential_bids[$result->bid], $result->status);
      } else {
        $potential_bids[$result->bid] = $result->status;
      }
    }
    
    $bids = array();
    // Only use the ones that are complete
    foreach ($potential_bids as $bid => $status) {
      if ($status == ICL_STATUS_SUCCESSFUL) {
        $bids[] = $bid;
      }
    }
    
  } elseif (isset($icl_content_dashboard_filter['translation']) && $icl_content_dashboard_filter['translation'] == 'in_progress') {

    $block_status_sql = "SELECT
                DISTINCT b.bid
	FROM
		{icl_block} b
            INNER JOIN {icl_block_status} bs ON b.bid = bs.bid 
            INNER JOIN {icl_core_status} c ON c.rid = bs.rid 

            WHERE c.status < %d";
    
    $bids = array();
    $in_progress_query = db_query($block_status_sql, ICL_STATUS_SUCCESSFUL);
    while ( $result = db_fetch_object ( $in_progress_query ) ) {
      $bids[] = $result->bid;
    }
    
  }

  if (isset($bids)) {
    $bids_to_use = implode(',', $bids);
    if ($bids_to_use != '') {
      $sql .= ' AND b.delta IN (' . $bids_to_use . ')';
    } else {
      $sql .= ' AND b.delta in (-1)'; // Select none
    }
  }  
}